let analyzeButton   = document.getElementById("analyzeButton");
let wordCountTable  = document.getElementById("wordCount");
let totalText       = document.getElementById("total");
let statsTable      = document.getElementById("stats");
let errorText       = document.getElementById("errorText");

function addEntiresToTable(table, ...entries) {
    let tr = document.createElement("tr");
    let td;
    for (const rawEntry of entries) {
        td = document.createElement("td");
        td.innerText = rawEntry;
        td.classList.add("tabletail");
        tr.appendChild(td);
    }
    table.appendChild(tr);
}

function sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}

analyzeButton.addEventListener("click", function() {
    errorText.innerText = "";
    function getDOMText() {
        return document.body.innerText;
    }
    try{
        chrome.tabs.executeScript({
            code: `(${getDOMText})();`
        }, (results) => {

            window.text = results[0];
        });
        sleep(100).then(() => {
            let counts = WBWC.count(window.text);
            delete window.text;
            for (const [word,count] of Object.entries(counts)) {
                if(count > 5)
                    addEntiresToTable(wordCountTable, word, count);
            }
            let stats = WBWC.analyze(counts);
            let ch = statsTable.children[0].children;
            ch[0].children[1].innerText = stats.mostCommon.join(", ") + " (" + stats.mostCommonAmt + ")";
            ch[1].children[1].innerText = stats.leastCommon.join(", ") + " (" + stats.leastCommonAmt + ")";
            ch[2].children[1].innerText = stats.longest.join(", ") + " (" + stats.longestLen + ")";
            ch[3].children[1].innerText = stats.shortest.join(", ") + " (" + stats.shortestLen + ")";
            ch[4].children[1].innerText = stats.avgCount;
            ch[5].children[1].innerText = stats.avgLen;
        });
    } catch (error) {
        errorText.innerText = "Something went wrong, try again!";
        console.error(error);
        return;
    }
});