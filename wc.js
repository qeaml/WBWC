// === Constants
const WORDSEP = "._ \t\n";
const LETTERS = "abcdefghijklmonpqrstuwvxyz";
const SINGLELETTERWORDS = "imuryna";
// module
const WBWC = {};

// === Helpers
function _toTextonly(text) {
    var out = "";
    for (const c of text) {
        if(LETTERS.indexOf(c) !== -1) out += c;
    }
    return out;
}

function _isUpper(text) {
    return text == text.toLocaleUpperCase();
}

function _isLower(text) {
    return text == text.toLocaleLowerCase();
}

// === API
WBWC.getWords = function (text) {
    var out = [], tmp = "", c;
    for (const c of text) {
        if(WORDSEP.indexOf(c) !== -1) {
            out.push(tmp);
            tmp = "";
            continue;
        }
        if(tmp.length > 0)
            if(_isLower(tmp[tmp.length-1]) && _isUpper(c)){
                out.push(tmp);
                tmp = "";
            }
        tmp += c;
    }
    out.push(tmp);
    let tmpW, realOut = [];
    for (let word of out) {
        word = _toTextonly(word.toLocaleLowerCase());
        if(word.length > 1) 
            realOut.push(word);
    }
    return realOut;
}

WBWC.count = function (text) {
    var words, out = {}, lines = text.split("\n");
    for (const ln of lines) {
        words = WBWC.getWords(ln);
        for (const word of words) {
            if(word in out) {
                out[word]++;
            } else {
                out[word] = 1;
            }
        }
    }
    return out;
}

WBWC.analyze = function (wordCounts) {
    var totalCountAll       = 0;// total number of all words
    var totalConutUnique    = 0;// total number of unique words
    var totalLength         = 0;// total length of all words
    var out = {
        mostCommon:     [],     // most common words
        mostCommonAmt:  0,      // amount of most common words
        leastCommon:    [],     // least common words
        leastCommonAmt: 0,      // amount of least common words
        longest:        [],     // longest word
        longestLen:     0,      // length of longest word
        shortest:       [],     // shortest word
        shortestLen:    0,      // length of shortest word
        avgCount:       0.0,    // average amount of unique words
        avgLen:         0.0     // average length of words
    };

    for (const [word, count] of Object.entries(wordCounts)) {
        totalConutUnique++;
        totalCountAll += count;
        totalLength += word.length;
        if(count > out.mostCommonAmt) {
            out.mostCommon = [word];
            out.mostCommonAmt = count;
        } else if(count === out.mostCommonAmt) {
            out.mostCommon.push(word);
        }
        if(word.length > out.longestLen) {
            out.longest = [word];
            out.longestLen = word.length;
        } else if(word.length === out.longestLen) {
            out.longest.push(word);
        }
    }
    out.avgCount = totalCountAll / totalConutUnique;
    out.avgLen = totalLength / totalConutUnique;

    out.leastCommonAmt = out.mostCommonAmt;
    out.shortestLen = out.longestLen;
    for (const [word, count] of Object.entries(wordCounts)) {
        if(count < out.leastCommonAmt) {
            out.leastCommon = [word];
            out.leastCommonAmt = count;
        } else if(count === out.leastCommonAmt) {
            out.leastCommon.push(word);
        }
        if(word.length < out.shortestLen) {
            out.shortest = [word];
            out.shortestLen = word.length;
        } else if(word.length === out.shortestLen) {
            out.shortest.push(word);
        }
    }

    return out;
}

// testing
// module.exports = WBWC;